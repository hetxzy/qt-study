#ifndef CHOOSELEVELSCENE_H
#define CHOOSELEVELSCENE_H

#include <QMainWindow>

class ChooseLevelScene : public QMainWindow
{
        Q_OBJECT
public:

    ChooseLevelScene();

    void paintEvent(QPaintEvent *);
signals:
    void chooseSceneBack();
};

#endif // CHOOSELEVELSCENE_H
