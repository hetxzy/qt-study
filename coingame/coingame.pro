#-------------------------------------------------
#
# Project created by QtCreator 2023-03-16T13:21:18
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = coingame
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    mypushbutton.cpp \
    chooselevelscene.cpp

HEADERS  += mainwindow.h \
    mypushbutton.h \
    chooselevelscene.h

FORMS    += mainwindow.ui

RESOURCES += \
    res.qrc
