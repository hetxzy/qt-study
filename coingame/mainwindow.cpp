#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QPainter>
#include <QPushButton>
#include "mypushbutton.h"
#include <qdebug.h>
#include <QTimer>
#include <chooselevelscene.h>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setFixedSize(320,588);
    this->setWindowIcon(QIcon(":/res/Coin0001.png"));
    this->setWindowTitle("翻金币主场景");


    connect(ui->actionQuit,&QAction::triggered,[=](){
        this->close();
    });

    //创建开始按钮
    MyPushButton * startBtn =new MyPushButton(":/res/MenuSceneStartButton.png");
    startBtn ->setParent(this);
    startBtn->move(this->width()*0.5-startBtn->width()*0.5,this->height() *0.7);

    chooseScene =new ChooseLevelScene;


    connect(startBtn,&MyPushButton::clicked,[=](){
        qDebug()<<"KASIH";
        startBtn->zoom1();
        startBtn->zoom2();

        this->hide();
        chooseScene->show();

    });
}


void MainWindow::paintEvent(QPaintEvent *)
{
    //创建画家，指定绘图设备
    QPainter painter(this);
     //创建QPixmap对象
    QPixmap pix;
    pix.load(":/res/PlayLevelSceneBg.png");
    painter.drawPixmap(0,0,this->width(),this->height(),pix);
    //加载标题
    pix.load(":/res/Title.png");
    //缩放图片
    pix = pix.scaled(pix.width()*0.5,pix.height()*0.5);

    painter.drawPixmap( 10,30,pix.width(),pix.height(),pix);
}

MainWindow::~MainWindow()
{
    delete ui;
}
