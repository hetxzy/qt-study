#include "mypushbutton.h"
#include <qdebug.h>
#include <QPropertyAnimation>
//MyPushButton::MyPushButton(QWidget *parent) :
//    QWidget(parent)
//{
//}
MyPushButton:: MyPushButton (QString normalImg,QString pressImg)
{
    //成员变量normalImgPath保存正常显示图片路径
    normalImgPath = normalImg;
    //成员变量pressedImgPath保存按下后显示的图片
    pressedImgPath = pressImg;
    //创建QPixmap对象
    QPixmap pixmap;

    bool ret = pixmap.load(normalImgPath);
    if(!ret)
    {
        qDebug() << normalImg << "加载图片失败";
    }
    this->setFixedSize(pixmap.width(),pixmap.height());
    this->setStyleSheet("QPushButton{border:0px}");

    this->setIcon(pixmap);
    this->setIconSize( QSize( pixmap.width(),pixmap.height() ) );


}
void MyPushButton::zoom1()
{
    QPropertyAnimation * animation1 = new QPropertyAnimation(this,"geometry");

    animation1 -> setDuration(200);
    //创建起始位置
    animation1->setStartValue(QRect(this->x(),this->y(),this->width(),this->height()));
    //创建结束位置
    animation1->setEndValue(QRect(this->x(),this->y()+10,this->width(),this->height()));
    animation1 ->setEasingCurve(QEasingCurve::OutBounce);
    animation1->start();
}

void MyPushButton::zoom2()
{
    QPropertyAnimation * animation1 =  new QPropertyAnimation(this,"geometry");
    animation1->setDuration(200);
    animation1->setStartValue(QRect(this->x(),this->y()+10,this->width(),this->height()));
    animation1->setEndValue(QRect(this->x(),this->y(),this->width(),this->height()));
    animation1->setEasingCurve(QEasingCurve::OutBounce);
    animation1->start();
}

void MyPushButton::mousePressEvent(QMouseEvent *e)
{
    if(this->pressedImgPath !="")
    {
        //创建QPixmap对象
        QPixmap pixmap;

        bool ret = pixmap.load(this->pressedImgPath);
        if(!ret)
        {
            qDebug() << "加载图片失败";
        }
        this->setFixedSize(pixmap.width(),pixmap.height());
        this->setStyleSheet("QPushButton{border:0px}");

        this->setIcon(pixmap);
        this->setIconSize( QSize( pixmap.width(),pixmap.height() ) );
        //交给父类执行按下事件

    }
        return QPushButton::mousePressEvent(e);

}

void MyPushButton::mouseReleaseEvent(QMouseEvent *e)
{
    if(normalImgPath != "") //选中路径不为空，显示选中图片
    {
        QPixmap pixmap;
        bool ret = pixmap.load(normalImgPath);
        if(!ret)
        {
            qDebug() << "加载图片失败";
        }
        this->setFixedSize( pixmap.width(), pixmap.height() );
        this->setStyleSheet("QPushButton{border:0px;}");
        this->setIcon(pixmap);
        this->setIconSize(QSize(pixmap.width(),pixmap.height()));
    }
    //交给父类执行 释放事件
    return QPushButton::mouseReleaseEvent(e);

}
