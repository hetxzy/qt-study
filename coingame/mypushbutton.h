#ifndef MYPUSHBUTTON_H
#define MYPUSHBUTTON_H

#include <QPushButton>

class MyPushButton : public QPushButton
{
    Q_OBJECT
public:
    //normalImg 代表正常显示的图片
    //pressImg  代表按下后显示的图片，默认为空

   // explicit MyPushButton(QWidget *parent = 0);
    MyPushButton (QString normalImg,QString pressImg = "");

    QString normalImgPath;
    QString pressedImgPath;

    void zoom1();//xia
    void zoom2();//shang

    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);

signals:

public slots:

};

#endif // MYPUSHBUTTON_H
